local states = {
    STATE_MAIN_MENU = 1,
    STATE_INGAME_CONSTRUCTION = 2,
    STATE_PAUSE_MENU = 3,
    STATE_MAIN_MENU_LOAD_SAVE = 4,
    STATE_SETTINGS = 5
}

return states
